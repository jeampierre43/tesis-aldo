<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioModel extends Model
{
    protected $table = 'users';
    protected $primaryKey='id';
    public $timestamps = false;

    protected $fillable = [
		'name',
		'apellidos',
    'email',
    'cedula',
    'edad',
    'id_rol_usuario',
    'password',
		'remember_token',
    'covid'
    ];
    
    protected $hidden = [
		'password',
		'remember_token'
    ];
    
    public function rol()
    {
        return $this->belongsToMany(\App\RolModel::class, 'rol_usuario','id_usuario','id_rol');
    }

    public function getNombresAttribute()
    {
        return "{$this->name} {$this->apellidos}";
    }
}
