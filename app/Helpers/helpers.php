<?php

use App\ingresoAreaModel;

function fechaSalida($ingreso){
  $ingreso =  ingresoAreaModel::where('id_usuario',$ingreso->id_usuario)
  ->where('id_area',$ingreso->id_area)->where('opcion',0)
  ->where('fecha_hora','>=',$ingreso->fecha_hora)->first();
  $fechaSalida = '';
  if ($ingreso) $fechaSalida = $ingreso->fecha_hora;

  return $fechaSalida;
}