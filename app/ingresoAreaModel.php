<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingresoAreaModel extends Model
{
    protected $table = 'ingreso_area';
    protected $primaryKey='id';
    public $timestamps = false;

    public function usuario()
    {
        return $this->belongsTo(\App\UsuarioModel::class,'id_usuario');
    }

    public function area()
    {
        return $this->belongsTo(\App\AreaModel::class,'id_area');
    }
}
