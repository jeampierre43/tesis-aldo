<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'Authenticate',
        'categorias',
        'productos',
        "ordenes",
        "ordenes/crear",
        "detalle",
        "detalle/crear",
        "detalle/eliminar",
        "venta/finalizar",
        "ordenes/eliminar",
        "cliente/crear",
        "cliente/listar",
        "usuario",
        "ingreso/crear",
        "ingreso/consulta",
        "ingreso/consultaPersonal",
        "consultaRol"
    ];
}
