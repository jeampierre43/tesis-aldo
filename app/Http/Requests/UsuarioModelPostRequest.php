<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioModelPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
            ],
            'apellidos' => [
                'present',
            ],
            'email' => [
                'required',
            ],
            'password' => [
                'required',
            ],
            'cedula' => [
                'present',
            ],
            'edad' => [
                'present',
            ],
            'id_rol_usuario' => [
                'required',
            ],
            'remember_token' => [
                'present',
            ],
            'email_verified_at' => [
                'present',
            ],
        ];
    }
}
