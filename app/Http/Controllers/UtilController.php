<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function fechaHora() {
        return  date('Y-m-d H:i:s');
    }

    public function Respuesta($respuesta, $message, $code) {
        return response()->json([
            'respuesta' => $respuesta,
            'message' => $message,
            'status' => ($code == 200) ? 'ok' : 'error'
        ], $code);
    }
}
