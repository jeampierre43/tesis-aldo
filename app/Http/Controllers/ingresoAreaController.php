<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ingresoAreaModel;
use App\AreaModel;
use App\Http\Controllers\UtilController;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ingresoAreaController extends Controller
{
	private $util;

	function __construct()
	{
		$this->util = new UtilController();
	}

	public function index()
    {
        $ingreso_areas = ingresoAreaModel::all();
        return view('ingreso_area.index', compact('ingreso_areas'));
	}
	
	public function historialIngresoUsuario($userId)
    {
		$request = request();
		$start_date= $request->start_date;
        $end_date= $request->end_date;
		if (is_null($start_date) && is_null($end_date)) {
			$ingreso_areas = ingresoAreaModel::where('id_usuario',$userId)->where('opcion',1)->get();
		}else{
			$ingreso_areas = ingresoAreaModel::where('id_usuario',$userId)->where('opcion',1)->whereBetween('fecha_hora',[$start_date,$end_date])->get();
		}
        
        return view('usuarios.historial_area', compact('ingreso_areas','userId'));
	}

	public function listaUsuarioIngresoArea($ingresoId)
    {
		$ingreso_areas = ingresoAreaModel::find($ingresoId);
		$fechaIngreso = $ingreso_areas->fecha_hora;
		$fechaSalida = fechaSalida($ingreso_areas);
		$usersId = ingresoAreaModel::whereBetween('fecha_hora',[$fechaIngreso,$fechaSalida])->pluck('id_usuario');
		$usuarios = User::whereIn('id',$usersId)->get();
        return view('usuarios.lista_usuarios_area', compact('usuarios','ingreso_areas'));
	}

	

	public function postIngreso(Request $request)
	{
		$this->validate($request, [
			'id_usuario' => 'integer|required',
			'id_area' => 'integer|required',
			'opcion' => 'integer|required'
		]);
		try {
			$id_area = $request->get('id_area');
			$area = AreaModel::find($id_area);
			if (empty($area)) {
				return $this->util->Respuesta([], 'El area a registrar no se encuentra en la base de datos.', 400);
			}
			$ingreso = new  ingresoAreaModel();
			$ingreso->id_usuario = $request->get('id_usuario');
			$ingreso->id_area = $id_area;
			$ingreso->opcion = $request->get('opcion');
			$ingreso->fecha_hora = $this->util->fechaHora();
			if ($ingreso->save()) {
				return $this->util->Respuesta($ingreso, 'Se creo la ingreso correctamente.', 200);
			}
		} catch (\Exception $ex) {
			return $this->util->Respuesta($ex, 'Algo a ocurrido ponerse en contadto con el administrador.', 500);
		}
	}

	public function getConsultaIngreso(Request $request)
	{
		try {
			$this->validate($request, [
				'id_usuario' => 'integer|required'
			]);
			$id_usuario = $request->get('id_usuario');
			$consulta = DB::table('ingreso_area as ia')
				->join('users as u', 'ia.id_usuario', '=', 'u.id')
				->join('area as ar', 'ia.id_area', '=', 'ar.id')
				->select('u.cedula', 'u.name', 'u.apellidos', 'ar.detalle as area', 'ia.fecha_hora as fecha')
				->where('ia.id_usuario', '=', $id_usuario)->orderBy('fecha_hora','desc')->get();
			return $this->util->Respuesta($consulta, 'Se consulto correctamente.', 200);
		} catch (\Exception $ex) {
			return $this->util->Respuesta($ex, 'Algo a ocurrido ponerse en contadto con el administrador.', 500);
		}
	}

	public function getConsultaPersonal(Request $request)
	{
		// return  $request->user()->tokens()->revoke();
		try {
			$this->validate($request, [
				'cedula' => 'string|required'
			]);
			$cedula = $request->get('cedula');
			$consulta = ['nombre' => 'IRVING L.', 'apellido' => 'ZAMBRANO O.', 'cedula' => '1313061275', 'estado' => true];
			return $this->util->Respuesta($consulta, 'Se consulto correctamente.', 200);
		} catch (\Exception $ex) {
			return $this->util->Respuesta($ex, 'Algo a ocurrido ponerse en contadto con el administrador.', 500);
		}
	}

	// public function getRolUsuario(Request $request)
	// {
	// 	// try {
	// 		// $this->validate($request, [
	// 		// 	'email' => 'string|required'
	// 		// ]);
	// 		return $email = $request->get('email');
	// 		$consulta = DB::table('rol_usuario as ru')
	// 			->join('users as u', 'ru.id_usuario', '=', 'u.id')
	// 			->join('rol as r', 'ru.id_rol', '=', 'r.id')->select('r.id', 'r.descripcion')->where('u.email', '=', $email)->get();
	// 		return $this->util->Respuesta($consulta, 'Se consulto correctamente.', 200);
	// 	// } catch (\Exception $ex) {
	// 	// 	return $this->util->Respuesta($ex, 'Algo a ocurrido ponerse en contadto con el administrador.', 500);
	// 	// }
	// }
}
