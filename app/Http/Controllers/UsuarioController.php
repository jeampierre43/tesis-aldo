<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UsuarioModelPostRequest;
use App\Http\Requests\UpdateUsuarioModelPostRequest;
use App\UsuarioModel;


class UsuarioController extends Controller
{

    public function index(Request $request)
    {
        $usuarios = UsuarioModel::all();

        if($request->ajax()){
            $param = $request->search;
            $fields = UsuarioModel::getModel()->getFillable();
            $usuarios = UsuarioModel::where(function($query) use($fields,$param){
                foreach ($fields as $field){
                    $query->orWhere('users.' . $field, 'like', '%'.$param .'%');
                }
            })->get();
            
            $table = '';
            foreach ($usuarios as $key => $usuario) {

                $table.='<tr>'.

                    '<td>'.$usuario->name.'</td>'.
                    '<td>'.$usuario->apellidos.'</td>'.
                    '<td>'.$usuario->email.'</td>'.
                    '<td>'.$usuario->cedula.'</td>'.
                    '<td>'.$usuario->edad.'</td>'.
                    '<td>'.$usuario->rol->map(function($rol) {
                            return "<span class='badge badge-primary'>".$rol->nombre."</span>";
                        })->implode('<br>').'</td>'.
                    '<td>'.
                        '<a href="'.url('ingreso-area/'.$usuario->id).'">Historial </a>'.
                        '<a href="'.route('usuarios.edit',['usuario'=>$usuario] ).'"> Editar</a>'.
                        '<a href="javascript:void(0)" onclick="event.preventDefault();'.
                        'document.getElementById("delete-usuario-model-'.$usuario->id.'").submit();">'.
                            ' Borrar'.
                        '</a>'.
                        '<form id="delete-usuario-model-'.$usuario->id.'" action="'.route('usuarios.destroy',['usuario'=>$usuario]).'" method="POST" style="display: none;">'.
                            
                        '</form>'.
                    '</td>'.
                
                '</tr>';
                
            }
            return Response($table);
        }
        return view('usuarios.index', compact('usuarios'));
    }

    public function show(Request $request, UsuarioModel $usuario)
    {
        return view('usuarios.show', compact('usuario'));
    }

    public function create()
    {
        return view('usuarios.create');
    }

    public function store(UsuarioModelPostRequest $request)
    {
        $data = $request->validated();
        $usuario = UsuarioModel::create($data);
        return redirect()->route('usuarios.index')->with('status', 'UsuarioModel created!');
    }

    public function edit(Request $request, UsuarioModel $usuario)
    {
        return view('usuarios.edit', compact('usuario'));
    }

    public function update(UpdateUsuarioModelPostRequest $request, UsuarioModel $usuario)
    {
        $data = $request->validated();
        $usuario->fill($data);
        $usuario->save();
        return redirect()->route('usuarios.index')->with('status', 'Usuarioss updated!');
    }

    public function destroy(Request $request, UsuarioModel $usuario)
    {
        $usuario->delete();
        return redirect()->route('usuarios.index')->with('status', 'Usuario destroyed!');
    }
}
