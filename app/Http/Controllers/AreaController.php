<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AreaModelPostRequest;
use App\AreaModel;


class AreaController extends Controller
{

    public function index()
    {
        $areas = AreaModel::all();
        return view('area.index', compact('areas'));
    }

    public function show(Request $request, AreaModel $area)
    {
        return view('area.show', compact('area'));
    }

    public function create()
    {
        return view('area.create');
    }

    public function store(AreaModelPostRequest $request)
    {
        $data = $request->validated();
        $area = AreaModel::create($data);
        $qrimage= public_path('/qr/area'.$area->id.'.png');
        \QRCode::text($area->id)->setOutfile($qrimage)->png(); 
        return redirect()->route('area.index')->with('status', 'Area created!');
    }

    public function edit(Request $request, AreaModel $area)
    {
        return view('area.edit', compact('area'));
    }

    public function update(AreaModelPostRequest $request, AreaModel $area)
    {
        $data = $request->validated();
        $area->fill($data);
        $area->save();
        if (!is_file(public_path('qr/'.$area->id.'.png'))) {
            $qrimage= public_path('qr/'.$area->id.'.png');
            \QRCode::text($area->id)->setOutfile($qrimage)->png();
        }
         
        return redirect()->route('area.index')->with('status', 'Area actualizada!');
    }

    public function destroy(Request $request, AreaModel $area)
    {
        $area->delete();
        return redirect()->route('area.index')->with('status', 'Area destroyed!');
    }
}
