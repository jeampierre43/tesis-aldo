<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolModel extends Model
{
    protected $table = 'rol';
    protected $primaryKey='id';
    public $timestamps = false;
}
