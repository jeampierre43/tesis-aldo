<?php

/*
  |--------------------------------------------------------------------------
  | Laravel CORS
  |--------------------------------------------------------------------------
  |
  | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
  | to accept any value.
  |
 */

return [
    'supportsCredentials' => true,
    'Access-Control-Allow-Origin' => ['http://localhost:4200'],
    'allowedOriginsPatterns' => ['http://localhost:4200'],
    'Access-Control-Allow-Headers' => ['AUTHORIZATION, X-REQUESTED-WITH, X-API-KEY, X-XSRF-TOKEN, X-PINGOTHER, Accept, Origin, Authorization, Content-Type, Access-Control-Request-Method'],
    'Access-Control-Allow-Methods' => ['GET, POST ,HEAD, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH'],
    'Access-Control-Expose-Headers' => ['X-My-Custom-Header, X-Another-Custom-Header'],
    'Access-Control-Max-Age' => 1000,
    'Content-Length' => 0,
    'Content-Type' => ['*'],
    'Content-Language' => ['*'],
    'Cache-Control' => ['*'],
    'X-Requested-With' => ['*'],
    'Pragma' => ['*'],
    'Expires' => 0,
];

//return [
//    'supportsCredentials' => true,
//    'allowedOrigins' => ['*'],
//    'allowedOriginsPatterns' => [],
//    'allowedHeaders' => ['*'],
//    'allowedMethods' => ['*'],
//    'exposedHeaders' => [],
//    'maxAge' => 1000,
//];

/*
  header("Access-Control-Allow-Origin:http://localhost:4200");
  header("Access-Control-Allow-Headers:AUTHORIZATION,X-REQUESTED-WITH,X-XSRF-TOKEN ");
  header("Access-Control-Allow-Methods: *");
  header("Access-Control-Max-Age: 1000");
  header("Content-Length: 0"); */















/*Access-Control-Allow-Origin: http://localhost:4200
Access-Control-Expose-Headers: *
Cache-Control: no-cache, private
Connection: Keep-Alive
Content-Length: 1456
Content-Type: text/html; charset=UTF-8
Date: Mon, 15 Oct 2018 01:39:32 GMT
Keep-Alive: timeout=5, max=100
Server: Apache/2.4.33 (Win32) OpenSSL/1.1.0h PHP/7.2.6
Set-Cookie: smartnbspinspectornbspservice_session=eyJpdiI6Ik9ha2dVUFlQTHV4eEl4S2NVd2haZ3c9PSIsInZhbHVlIjoiUWRSbTZpd29qczA2YlpwMzdFM2N0c0pPUFhEUGYzNWVMOWs3Y3ZSczdpdE5hYWJCZ1ZhaXJcLzNlVEJ4TGU2c3QiLCJtYWMiOiJiM2FmMWU0OWYwMjBhNzNhNGIwMzRiMmIyODNiYWFkMDQ4MmIwZTVjMDA5MzkxNzcxYTc4ZjI4YmMwOTIzNDhlIn0%3D; expires=Mon, 15-Oct-2018 03:39:33 GMT; Max-Age=7200; path=/; httponly
Set-Cookie: XSRF-TOKEN=eyJpdiI6InZxZnFkZUlWd216dEcybUc4SmNIaVE9PSIsInZhbHVlIjoiVjhTdDlCNmVHT0RROHV6Q2oyTThCQ0NIbHVtRWtMUU93SnNCOVkrSzM0SVVLMXZ1Z05QVXBSRzRFN2VURmNFcSIsIm1hYyI6Ijg2Y2EwYTQ3ZjBlNmMwOTNiZGNmZTY2ODczMWVmNDI0NTRkNGZkOWU3ZmIyN2Q4MDgxMjY3ZTdmZTU4OGU0OWIifQ%3D%3D; expires=Mon, 15-Oct-2018 03:39:33 GMT; Max-Age=7200; path=/
Vary: Authorization,Origin
X-Powered-By: PHP/7.2.6*/

/*Access-Control-Allow-Headers: AUTHORIZATION,X-REQUESTED-WITH,X-XSRF-TOKEN
Access-Control-Allow-Methods: POST
Access-Control-Allow-Origin: http://localhost:4200
Access-Control-Max-Age: 1000
Cache-Control: no-cache, private
Connection: close
Content-Length: 0
Content-Type: text/html; charset=UTF-8
Date: Mon, 15 Oct 2018 01:39:32 GMT
Server: Apache/2.4.33 (Win32) OpenSSL/1.1.0h PHP/7.2.6
X-Powered-By: PHP/7.2.6*/

