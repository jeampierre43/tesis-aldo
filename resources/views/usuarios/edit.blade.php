@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card">

        <div class="header">
            <h3> Editar Usuario </h3>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('usuarios.update',['usuario'=>$usuario->id])}}" method="POST" novalidate>
        {{Form::token()}}
        {{ method_field('PUT') }}
        

        <div class="form-group">
            <label for="name">Nombre</label>
            <input class="form-control String"  type="text"  name="name" id="name" value="{{old('name',$usuario->name)}}"required="required">
            @if($errors->has('name'))
            <p class="text-danger">{{$errors->first('name')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="apellidos">Apellidos</label>
            <input class="form-control String"  type="text"  name="apellidos" id="apellidos" value="{{old('apellidos',$usuario->apellidos)}}">
            @if($errors->has('apellidos'))
            <p class="text-danger">{{$errors->first('apellidos')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control String"  type="text"  name="email" id="email" value="{{old('email',$usuario->email)}}"required="required">
            @if($errors->has('email'))
            <p class="text-danger">{{$errors->first('email')}}</p>
            @endif
        </div>
                                
        <div class="form-group">
            <label for="cedula">Cedula</label>
            <input class="form-control String"  type="text"  name="cedula" id="cedula" value="{{old('cedula',$usuario->cedula)}}">
            @if($errors->has('cedula'))
            <p class="text-danger">{{$errors->first('cedula')}}</p>
            @endif
        </div>
        <div class="form-group col-md-10">
            <label for="edad">Edad</label>
            <input class="form-control String"  type="text"  name="edad" id="edad" value="{{old('edad',$usuario->edad)}}">
            @if($errors->has('edad'))
            <p class="text-danger">{{$errors->first('edad')}}</p>
            @endif
        </div>
        
            <div class="form-group text-center col-md-2">
              {!! Form::label('covid', 'Enfermo') !!}
              <div class="">
                <label for="">
                  {!! Form::hidden('covid', 0, null) !!}
                  {!! Form::checkbox('covid', 1,$usuario->covid) !!}
                </label>
                
              </div>
              
            </div>
        
        <div class="form-group">
            <label for="id_rol_usuario">Rol Usuario</label>
            {!! Form::select('id_rol_usuario', App\RolModel::pluck('nombre','id'), old('id_rol_usuario',$usuario->id_rol_usuario), ['class'=>'form-control']) !!}
            @if($errors->has('id_rol_usuario'))
            <p class="text-danger">{{$errors->first('id_rol_usuario')}}</p>
            @endif
        </div>
                                
                                
        <div class="">
            <button class="btn btn-primary btn-fill" type="submit">Guardar</button>
            <a href="{{ url()->previous() }}" class="btn-default btn">Regresar</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection