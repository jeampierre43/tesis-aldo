@extends('layouts.master')
@section('contenido')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="header">
            <h4 class="title">Lista de usuario en el area {{$ingreso_areas->area->nombre}}</h4>
        </div>
        <div class="card-body">

            <div style="padding-right: 30px;">
                <a href="{{route('usuarios.create')}}" class="btn btn-fill btn-primary pull-right">Nuevo</a>
            </div>
            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped">
                    @if(count($usuarios))
                    <thead>
                        <tr>
                                            
                            <th>Nombres</th>

                            <th>Apellidos</th>

                            <th>Email</th>

                            <th>Cedula</th>

                            <th>Edad</th>

                            <th>Rol</th>

                            <th>&nbsp;</th>

                        </tr>

                    </thead>
                    @endif
                    <tbody>
                        @forelse($usuarios as $usuario)
                        <tr>
                            
                            <td>{{$usuario->name}}</td>
                            <td>{{$usuario->apellidos}}</td>
                            <td>{{$usuario->email}}</td>
                            <td>{{$usuario->cedula}}</td>
                            <td>{{$usuario->edad}}</td>
                            <td>
                                @isset($usuario->rol)
                                {!!$usuario->rol->map(function($rol) {
                                    return "<span class='badge badge-primary'>".$rol->nombre."</span>";
                                })->implode('<br>')!!}
                                @endisset
                            </td>
                            <td>
                               
                            </td>
                                                                                            
                        </tr>
                        @empty
                        <p>No hay Usuarios</p>
                        @endforelse
                    </tbody>
                </table> 
            </div>
            
        </div>
    </div>

</div>

@endsection