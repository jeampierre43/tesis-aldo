@extends('layouts.master')
@section('contenido')
<div class="container">

    <div class="card mb-4">

        <div class="card-header">
            <h1> Ver Usuario Model  </h1>
        </div>

    <div class="card-body">
                                        <div class="form-group">
            <label class="col-form-label" for="value">Name</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->name}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Apellidos</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->apellidos}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Email</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->email}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Password</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->password}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Cedula</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->cedula}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Edad</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->edad}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Id Rol Usuario</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->id_rol_usuario}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Remember Token</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$usuario->remember_token}}">
        </div>
                                                                    </div>

    </div>

    <div class="card mb-4">

        
    </div>



    <a href="{{ url()->previous() }}">Regresar</a>
</div>
@endsection