@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card stacked-form">

        <div class="header">
            <h3 class="card-title"> Crear Usuario </h3>
        </div>
        <div class="card-body">

            @if($errors->any())
            <ul>
                @foreach($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
                @endforeach
            </ul> 
            @endif 
            <form action="{{route('usuarios.store')}}" method="POST" novalidate>
                {{Form::token()}}
                <div class="form-group">
                <label for="name">Nombre</label>
                <input class="form-control String"  type="text"  name="name" id="name" value="{{old('name')}}"             maxlength="191"
                    required="required">
                @if($errors->has('name'))
                <p class="text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>
                                    <div class="form-group">
                <label for="apellidos">Apellidos</label>
                            <input class="form-control String"  type="text"  name="apellidos" id="apellidos" value="{{old('apellidos')}}"             maxlength="255"
                                        >
                            @if($errors->has('apellidos'))
                <p class="text-danger">{{$errors->first('apellidos')}}</p>
                @endif
            </div>
                                    <div class="form-group">
                <label for="email">Email</label>
                            <input class="form-control String"  type="text"  name="email" id="email" value="{{old('email')}}"             maxlength="191"
                                        required="required"
                            >
                            @if($errors->has('email'))
                <p class="text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>
                                    <div class="form-group">
                <label for="password">Password</label>
                            <input class="form-control String"  type="text"  name="password" id="password" value="{{old('password')}}"             maxlength="191"
                                        required="required"
                            >
                            @if($errors->has('password'))
                <p class="text-danger">{{$errors->first('password')}}</p>
                @endif
            </div>
                                    <div class="form-group">
                <label for="cedula">Cedula</label>
                            <input class="form-control String"  type="text"  name="cedula" id="cedula" value="{{old('cedula')}}"             maxlength="13"
                                        >
                            @if($errors->has('cedula'))
                <p class="text-danger">{{$errors->first('cedula')}}</p>
                @endif
            </div>
                                    <div class="form-group">
                <label for="edad">Edad</label>
                            <input class="form-control String"  type="text"  name="edad" id="edad" value="{{old('edad')}}"             maxlength="3"
                                        >
                            @if($errors->has('edad'))
                <p class="text-danger">{{$errors->first('edad')}}</p>
                @endif
            </div>
            
            <div class="form-group">
                <label for="id_rol_usuario">Rol Usuario</label>
                {!! Form::select('id_rol_usuario', App\RolModel::pluck('nombre','id'), old('id_rol_usuario'), ['class'=>'form-control']) !!}
                @if($errors->has('id_rol_usuario'))
                <p class="text-danger">{{$errors->first('id_rol_usuario')}}</p>
                @endif
            </div>
                                    
            <div class="text-right">
                <button class="btn btn-fill btn-primary" type="submit">Crear</button>
                <a href="{{ url()->previous() }}">Regresar</a>
            </div>
            </form>
        </div>
    </div>
    
</div>
@endsection

