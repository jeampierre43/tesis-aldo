@extends('layouts.master')
@section('contenido')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="header">
            <h3> Historial de Ingreso Areas de @if(count($ingreso_areas)){{$ingreso_areas->first()->usuario->nombres}}@endif </h3>
        </div>
        {!! Form::open(['route' => ['historialIngresoUsuario','userId' => $userId]]) !!}
        <div style="padding: 0 4px;" class="row">
            <div class="col-sm-6">
                <!-- Start date and time Field -->
                <div class="form-group ">
                    {!! Form::label('start_date', 'Fecha inicio', ['class' => 'control-label text-right']) !!}
                    <div class="col-6">
                        {!! Form::date('start_date', isset($start_date)?date("Y-m-d",strtotime($start_date)):null,  ['class' => 'form-control datepicker','autocomplete'=>'off','placeholder'=>  trans("lang.delivery_discount_star_date_time_placeholder")  ]) !!}
                    
                    </div>
                </div>
            </div>
            
            <div class="col-6">
                <!-- End date and time Field -->
                <div class="form-group col-6">
                    {!! Form::label('end_date', 'Fecha Fin', ['class' => 'control-label text-right']) !!}
                    <div class="col-sm-6">
                        {!! Form::date('end_date', isset($end_date)?date("Y-m-d",strtotime($end_date)):null,  ['class' => 'form-control datepicker','autocomplete'=>'off','placeholder'=>  trans("lang.delivery_discount_end_date_time_placeholder")  ]) !!}
                    
                    </div>
                </div>
            </div>
            

            

            <div class="col-sm-12 text-right">
                <button type="submit" class="btn btn-fill btn-primary ">Buscar <i class="fa fa-search"></i></button>
            </div>
            
        </div>
        {!! Form::close() !!}

        <div class="card-body">

            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    @if(count($ingreso_areas))
                    <thead>
                        <tr>
                                                 
                            <th>Codigo</th>

                            <th>Area</th>

                            <th>Entrada  -  Salida </th>
                            <th>&nbsp;</th>
                            
                        </tr>

                    </thead>
                    @endif
                    <tbody>
                        @forelse($ingreso_areas as $ingreso_area)
                        <tr>
                            
                            <td>{{$ingreso_area->area->codigo}}</td>
                            <td>{{$ingreso_area->area->nombre}}</td>
                            <td>{{$ingreso_area->fecha_hora.'  -  '.fechaSalida($ingreso_area)}}</td>
                            <td><a href="{{url('ingreso-area/lista/usuarios/'.$ingreso_area->id)}} ">Lista de usuarios</a></td>
                            
                        </tr>
                        @empty
                        <p>No Ingreso Area</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

</div>

@endsection