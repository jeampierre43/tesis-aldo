@extends('layouts.master')
@section('contenido')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="header">
            <h4 class="title">Usuarios</h4>
        </div>
        <div class="card-body">

            <div style="padding-right: 30px;">
                <div class="form-group">
                    <div class="col-3">
                        <input type="text" class="form-control pull-left" id="search" name="search" style="width: 30%" placeholder="Buscar usuario">
                    </div>
                               
                </div>
                <a href="{{route('usuarios.create')}}" class="btn btn-fill btn-primary pull-right">Nuevo</a>
            </div>
            <br><br>
            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped">
                    @if(count($usuarios))
                    <thead>
                        <tr>
                                            
                            <th>Nombres</th>

                            <th>Apellidos</th>

                            <th>Email</th>

                            <th>Cedula</th>

                            <th>Edad</th>

                            <th>Rol</th>

                            <th>&nbsp;</th>

                        </tr>

                    </thead>
                    @endif
                    <tbody>
                        @forelse($usuarios as $usuario)
                        <tr>
                            
                            <td>{{$usuario->name}}</td>
                            <td>{{$usuario->apellidos}}</td>
                            <td>{{$usuario->email}}</td>
                            <td>{{$usuario->cedula}}</td>
                            <td>{{$usuario->edad}}</td>
                            <td>{!!$usuario->rol->map(function($rol) {
                                return "<span class='badge badge-primary'>".$rol->nombre."</span>";
                            })->implode('<br>')!!}</td>
                            <td>
                                <a href="{{url('ingreso-area/'.$usuario->id)}} ">Historial</a>
                                <a href="{{route('usuarios.edit',['usuario'=>$usuario] )}}">Editar</a>
                                <a href="javascript:void(0)" onclick="event.preventDefault();
                                document.getElementById('delete-usuario-model-{{$usuario->id}}').submit();">
                                    Borrar
                                </a>
                                <form id="delete-usuario-model-{{$usuario->id}}" action="{{route('usuarios.destroy',['usuario'=>$usuario])}}" method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                                                                                            
                        </tr>
                        @empty
                        <p>No hay Usuarios</p>
                        @endforelse
                    </tbody>
                </table> 
            </div>
            
        </div>
    </div>

</div>
@endsection

@section('page-js')
<script>
    $('#search').on('keyup',function(){

    $value=$(this).val();

    $.ajax({
        type : 'get',
        url : '{{URL::to('usuarios')}}',
        data:{'search':$value},
        success:function(data){
            $('tbody').html(data);
        }

    });



    })
</script>
@endsection