<style type="text/css">
    .sidebar[data-color="negro"]:after {
        background: linear-gradient(to bottom, #000 0%, #23239a 100%);
    }
</style>
<div class="sidebar" data-color="negro" data-image="{{asset('assets/img/fondo_login.jpg')}}" >

    <!--{{asset('assets/img/fondo_login.jpg')}} {{asset('admin/assets/img/sidebar-5.jpg')}}

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->
    <?php //$categorias = App\equipoModel::select('categoria')->groupBy('categoria')->orderBy('categoria', 'desc')->get(); ?>
    
	<div class="sidebar-wrapper">
        
        <div class="logo logo-side-bar">
            <a href="{{url('/')}}" class="simple-text">
                {{-- <img src="{{asset('assets/img/logo_campeonato_SF.png')}}" alt=""> --}}
                <h3>QR System</h3>
            </a>
        </div>
        
        <ul id="myUL" class="nav">
            
            <li>
                <a href="{{url('usuarios')}}">
                    <i class="pe-7s-user"></i>
                    <p>Usuarios</p>
                </a>
            </li>
            <li>
                <a href="{{url('area')}}">
                    <i class="pe-7s-note2"></i>
                    <p>Areas</p>
                </a>
            </li>
            <li>
                <a href="{{url('ingreso-area')}}">
                    <i class="pe-7s-news-paper"></i>
                    <p>Historial</p>
                </a>
            </li>
            {{-- <li>
                <a href="icons.html">
                    <i class="pe-7s-science"></i>
                    <p>Icons</p>
                </a>
            </li>
            <li>
                <a href="maps.html">
                    <i class="pe-7s-map-marker"></i>
                    <p>Maps</p>
                </a>
            </li>
            <li>
                <a href="notifications.html">
                    <i class="pe-7s-bell"></i>
                    <p>Notifications</p>
                </a>
            </li>
			<li class="active-pro">
                <a href="upgrade.html">
                    <i class="pe-7s-rocket"></i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> --}}
        </ul>
	</div>
</div>

<style type="text/css">
    .logo-side-bar img {
        width: 180px;
        height: 115px;
    }

    .sidebar-wrapper p{
        margin: 0;
        line-height: 30px;
        font-size: 12px;
        font-weight: 600;
        text-transform: uppercase;
        margin-left: 45px;
        color: #FFFFFF;
    }
</style>

