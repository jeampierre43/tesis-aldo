@extends('layouts.master')
@section('contenido')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="header">
            <h3> Areas </h3>
        </div>
        <div class="card-body">

            <div style="padding-right: 30px;">
                <a href="{{route('area.create')}}" class="btn btn-fill btn-primary pull-right">Nuevo</a>
            </div>
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    @if(count($areas))
                    <thead>
                        <tr>
                            
                                                        
                                                                                    <td>Codigo</td>
                            
                                                                                    <td>Nombre</td>
                            
                                                                                    <td>Detalle</td>
                            
                                                        <th>&nbsp;</th>
                        </tr>

                    </thead>
                    @endif
                    <tbody>
                        @forelse($areas as $area)
                        <tr>
                            
                                                                                                                                            <td>{{$area->codigo}}</td>
                                                                                                                <td>{{$area->nombre}}</td>
                                                                                                                <td>{{$area->detalle}}</td>
                                                                                    <td>
                                <a href="{{route('area.show',['area'=>$area] )}}">Ver</a>
                                <a href="{{route('area.edit',['area'=>$area] )}}">Editar</a>
                                <a href="javascript:void(0)" onclick="event.preventDefault();
                                document.getElementById('delete-area-model-{{$area->id}}').submit();">
                                    Borrar
                                </a>
                                <form id="delete-area-model-{{$area->id}}" action="{{route('area.destroy',['area'=>$area])}}" method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>

                        </tr>
                        @empty
                        <p>No Area Models</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

</div>

@endsection