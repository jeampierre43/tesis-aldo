@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card">

        <div class="header">
            <h3> Crear Area  </h3>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul> @endif <form action="{{route('area.store')}}" method="POST" novalidate>
        {{Form::token()}}
        
                                        <div class="form-group">
            <label for="codigo">Codigo</label>
                        <input class="form-control String"  type="text"  name="codigo" id="codigo" value="{{old('codigo')}}"             maxlength="255"
                                    >
                        @if($errors->has('codigo'))
            <p class="text-danger">{{$errors->first('codigo')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                        <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre')}}"             maxlength="255"
                                    >
                        @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="detalle">Detalle</label>
                        <input class="form-control String"  type="text"  name="detalle" id="detalle" value="{{old('detalle')}}"             maxlength="255"
                                    >
                        @if($errors->has('detalle'))
            <p class="text-danger">{{$errors->first('detalle')}}</p>
            @endif
        </div>
                        <div class="text-right">
            <button class="btn btn-fill btn-primary" type="submit">Crear</button>
            <a href="{{ url()->previous() }}">Regresar</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection
