@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card">

        <div class="header">
            <h3> Editar Area  </h3>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('area.update',['area'=>$area->id])}}" method="POST" novalidate>
        {{Form::token()}}
        {{ method_field('PUT') }}
        @if(is_file(public_path('qr/'.$area->id.'.png')))
        <img src='{{asset('qr/'.$area->id.'.png')}}' style="height: 130px;">
        @endif                                <div class="form-group">
            <label for="codigo">Codigo</label>
                    <input class="form-control String"  type="text"  name="codigo" id="codigo" value="{{old('codigo',$area->codigo)}}"
                                    >
                    @if($errors->has('codigo'))
            <p class="text-danger">{{$errors->first('codigo')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                    <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre',$area->nombre)}}"
                                    >
                    @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="detalle">Detalle</label>
                    <input class="form-control String"  type="text"  name="detalle" id="detalle" value="{{old('detalle',$area->detalle)}}"
                                    >
                    @if($errors->has('detalle'))
            <p class="text-danger">{{$errors->first('detalle')}}</p>
            @endif
        </div>
                        <div class="text-right">
            <button class="btn btn-fill btn-primary" type="submit">Guardar</button>
            <a href="{{ url()->previous() }}">Regresar</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection