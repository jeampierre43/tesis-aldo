@extends('layouts.master')
@section('contenido')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="header">
            <h3> Ingreso Area </h3>
        </div>
        <div class="card-body">

            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    @if(count($ingreso_areas))
                    <thead>
                        <tr>
                                                 
                            <td>Usuario</td>

                            <td>Area</td>

                            <td>Fecha Hora</td>
                            
                            
                        </tr>

                    </thead>
                    @endif
                    <tbody>
                        @forelse($ingreso_areas as $ingreso_area)
                        <tr>
                            
                            <td>{{$ingreso_area->usuario->nombres}}</td>
                            <td>{{$ingreso_area->area->nombre}}</td>
                            <td>{{$ingreso_area->fecha_hora}}</td>
                            

                        </tr>
                        @empty
                        <p>No Ingreso Area</p>
                        @endforelse
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>

</div>

@endsection