@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card">

        <div class="header">
            <h3> Crear Ingreso Area Model  </h3>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul> @endif <form action="{{route('ingreso-area-models.store')}}" method="POST" novalidate>
        @csrf
        
                                        <div class="form-group">
            <label for="id_usuario">Id Usuario</label>
                        <input class="form-control BigInt"  type="text"  name="id_usuario" id="id_usuario" value="{{old('id_usuario')}}"                         >
                        @if($errors->has('id_usuario'))
            <p class="text-danger">{{$errors->first('id_usuario')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="id_area">Id Area</label>
                        <input class="form-control BigInt"  type="text"  name="id_area" id="id_area" value="{{old('id_area')}}"                         >
                        @if($errors->has('id_area'))
            <p class="text-danger">{{$errors->first('id_area')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="opcion">Opcion</label>
                        <input class="form-control String"  type="text"  name="opcion" id="opcion" value="{{old('opcion')}}"             maxlength="255"
                                    >
                        @if($errors->has('opcion'))
            <p class="text-danger">{{$errors->first('opcion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="fecha_hora">Fecha Hora</label>
                        <input class="form-control DateTime"  type="text"  name="fecha_hora" id="fecha_hora" value="{{old('fecha_hora')}}"                         >
                        @if($errors->has('fecha_hora'))
            <p class="text-danger">{{$errors->first('fecha_hora')}}</p>
            @endif
        </div>
                        <div class="text-right">
            <button class="btn btn-fill btn-primary" type="submit">Crear</button>
            <a href="{{ url()->previous() }}">Regresar</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection
