@extends('layouts.master')
@section('contenido')
<div class="container">
    <div class="card">

        <div class="header">
            <h3> Editar Ingreso Area Model  </h3>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('ingreso-area-models.update',['ingreso_area_model'=>$ingreso_area_model->id])}}" method="POST" novalidate>
        @csrf
        @method('PUT')
        

                                        <div class="form-group">
            <label for="id_usuario">Id Usuario</label>
                    <input class="form-control BigInt"  type="text"  name="id_usuario" id="id_usuario" value="{{old('id_usuario',$ingreso_area_model->id_usuario)}}"
                                    >
                    @if($errors->has('id_usuario'))
            <p class="text-danger">{{$errors->first('id_usuario')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="id_area">Id Area</label>
                    <input class="form-control BigInt"  type="text"  name="id_area" id="id_area" value="{{old('id_area',$ingreso_area_model->id_area)}}"
                                    >
                    @if($errors->has('id_area'))
            <p class="text-danger">{{$errors->first('id_area')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="opcion">Opcion</label>
                    <input class="form-control String"  type="text"  name="opcion" id="opcion" value="{{old('opcion',$ingreso_area_model->opcion)}}"
                                    >
                    @if($errors->has('opcion'))
            <p class="text-danger">{{$errors->first('opcion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="fecha_hora">Fecha Hora</label>
                    <input class="form-control DateTime"  type="text"  name="fecha_hora" id="fecha_hora" value="{{old('fecha_hora',$ingreso_area_model->fecha_hora)}}"
                                    >
                    @if($errors->has('fecha_hora'))
            <p class="text-danger">{{$errors->first('fecha_hora')}}</p>
            @endif
        </div>
                        <div class="text-right">
            <button class="btn btn-fill btn-primary" type="submit">Guardar</button>
            <a href="{{ url()->previous() }}">Regresar</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection