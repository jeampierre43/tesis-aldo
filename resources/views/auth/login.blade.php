<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="icon" href="{{ asset('public/assets/img/qr-logo.png') }}" type="image/x-icon" />
    <title>Inicio de Sesi&oacute;n | Qr System </title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/styles/vendor/toastr.css')}}">
        <link rel="icon" href="{{ asset('assets/img/qr-logo.png') }}" type="image/x-icon" />
        <style type="text/css">
        	.login-logo img {
			    width: 190px;
			    height: 150px;
			}
        </style>
</head>

<body>
    <div class="auth-layout-wrap" style="background-image: url({{asset('assets/img/tech-planet.jpg')}})">
        <div class="auth-content">
            <div class="card o-hidden">
                <div class="row">
                  <div class="col-md-12">
                        <div class="p-4 text-center">
                            <div class="login-logo text-center ">
                                <img src="{{asset('assets/img/qr-logo.png')}}" alt="">
                                <h2>QR System</h2>
                            </div>
                            <br>
                              <h1 class="mb-3 text-18"> Inicio de Sesi&oacute;n</h1>
                            <form action="{{ route('login') }}" method="POST" >
                                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                                <div class="form-group ">
                                    <label for="email">Ingrese su email</label>
                                    <input id="email" name="email" class="form-control" type="text" required>
                                </div>
                                <div class="form-group">
                                    <label for="password">Ingrese su contrase&ntilde;a</label>
                                    <input id="password" name="password" class="form-control form-control-rounded" type="password" required>
                                </div>
                                
                                <button  class="text-center btn btn-rounded btn-primary "> Iniciar Sesi&oacute;n</button>
                                <!-- <input id="campeonato" name="campeonato" type="hidden"> -->
                              <p class="lost_password">
                        <a href="{{ url('/') }}" >Regresar al inicio </a>
                    </p>
                            </form>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
 <script src="{{asset('assets/js/vendor/toastr.min.js')}}"></script>
 <script>
     @if(Session::has('message-error'))
     toastr.error("{{Session::get('message-error')}}")
     @endif
     @if(Session::has('message'))
     toastr.success("{{Session::get('message')}}", {
                timeOut: "50000",
            })
     @endif
 </script>
</body>
</html>
