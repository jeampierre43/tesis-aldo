<?php


Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::post('consultaRol','AuthenticateController@getRolUsuario')->name('consultaRol');//getRolUsuario
Route::post('Authenticate', 'AuthenticateController@Authenticate')->name('Authenticate');

Route::resource('usuarios', 'UsuarioController');
Route::resource('area', 'AreaController');
Route::get('ingreso-area', 'ingresoAreaController@index');
Route::any('ingreso-area/{user}', 'ingresoAreaController@historialIngresoUsuario')->name('historialIngresoUsuario');
Route::get('ingreso-area/lista/usuarios/{ingreso}', 'ingresoAreaController@listaUsuarioIngresoArea');

Route::group(['middleware' => 'jwt.auth'], function () {

	Route::post('ingreso/crear','ingresoAreaController@postIngreso');//getConsultaIngreso
	Route::post('ingreso/consulta','ingresoAreaController@getConsultaIngreso');//getConsultaIngreso
	Route::post('ingreso/consultaPersonal','ingresoAreaController@getConsultaPersonal');//getConsultaPersonal
	

});



